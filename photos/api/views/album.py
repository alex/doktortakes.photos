from django.core.cache import cache
from django.http import Http404

from rest_framework import exceptions, permissions
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from photos.api.serializers import (
    AlbumSerializer, AlbumCoverSerializer, PhotoSerializer)
from photos.models import Album, Photo
from photos.models.utils import generate_md5_hash, CHUNK_SIZE
from photos.utils import get_album_for_user_or_404, get_albums_for_user

from http import HTTPStatus as Status
from io import BytesIO


class AlbumNotFound(exceptions.APIException):
    status_code = Status.NOT_FOUND

    def __init__(self):
        super().__init__("Album not found.")


class AlbumList(APIView):
    @staticmethod
    def get(request: Request) -> Response:
        albums = (get_albums_for_user(request.user, include_children=True)
                  .select_related('cover', 'parent')
                  .prefetch_related('children', 'tags', 'users', 'groups'))
        serializer = AlbumSerializer(albums, many=True)

        return Response({'albums': serializer.data})

    @staticmethod
    def post(request: Request) -> Response:
        if not request.user.is_staff:
            raise exceptions.PermissionDenied("Not authorized.")

        serializer = AlbumSerializer(data=request.data)

        if serializer.is_valid():
            album = serializer.save()
            response = {'path': album.path}

            return Response(response, status=Status.CREATED)

        return Response(serializer.errors, status=Status.BAD_REQUEST)


class AlbumDetail(APIView):
    @staticmethod
    def get_album(request: Request, path: str) -> Album:
        try:
            album = get_album_for_user_or_404(request, path)
        except Http404:
            raise AlbumNotFound

        if request.method not in permissions.SAFE_METHODS and not request.user.is_staff:
            raise exceptions.PermissionDenied("Not authorized.")

        return album

    # Methods

    def get(self, request: Request, path: str) -> Response:
        album = self.get_album(request, path)
        serializer = AlbumSerializer(album)

        return Response(serializer.data)

    def patch(self, request: Request, path: str) -> Response:
        album = self.get_album(request, path)
        serializer = AlbumCoverSerializer(album, data=request.data)

        if serializer.is_valid():
            serializer.save()

            photo = PhotoSerializer(album.cover)
            return Response(photo.data)

        return Response(serializer.errors, status=Status.BAD_REQUEST)

    def put(self, request: Request, path: str) -> Response:
        album = self.get_album(request, path)
        serializer = AlbumSerializer(album, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=Status.BAD_REQUEST)

    def delete(self, request: Request, path: str) -> Response:
        album = self.get_album(request, path)
        album.delete()

        return Response(None, status=Status.NO_CONTENT)


class AlbumPhotoList(APIView):
    @staticmethod
    def get_album(request: Request, path: str) -> Album:
        try:
            return get_album_for_user_or_404(request, path)
        except Http404:
            raise AlbumNotFound

    def get(self, request: Request, path: str) -> Response:
        album = self.get_album(request, path)

        response = []
        photos = album.photos.filter(sidecar_exists=True).order_by('taken')

        for index, photo in enumerate(photos):
            context = {
                'index': index,
                'is_staff': request.user.is_staff,
            }
            response.append(PhotoSerializer(photo, context=context).data)

        return Response({'photos': response}, status=Status.OK)

    def post(self, request: Request, path: str) -> Response:
        if not request.user.is_staff:
            raise AlbumNotFound

        files = request.FILES.getlist('files')
        album = self.get_album(request, path)

        photos = []

        for file in files:
            photo = Photo()
            photo.album = album

            # Check if this image already exists
            md5 = generate_md5_hash(file)

            try:
                Photo.objects.get(md5=md5)
            except Photo.DoesNotExist:
                pass
            else:
                raise exceptions.ValidationError(f"Duplicate file: {md5}")

            photo.md5 = md5

            filename = file.name

            try:
                original = Photo.objects.get(album=album,
                                             original_filename=filename)
            except Photo.DoesNotExist:
                pass
            else:
                original.delete()

            data = BytesIO()
            data.name = filename

            photo.original_filename = filename

            for chunk in file.chunks(chunk_size=CHUNK_SIZE):
                data.write(chunk)

            # Expires after 24 hours
            cache.set(md5, data, 60 * 60 * 24)

            photo.save()
            photos.append(PhotoSerializer(photo).data)

        return Response({'photos': photos}, status=Status.OK)

    def delete(self, request: Request, path: str) -> Response:
        if not request.user.is_staff:
            raise exceptions.PermissionDenied("Not authorized.")

        album = self.get_album(request, path)

        try:
            hashes = request.data['photos']
        except KeyError:
            raise exceptions.ValidationError("No photos were specified.")

        for photo in album.photos.filter(md5__in=hashes):
            photo.delete()

        return Response(status=Status.NO_CONTENT)
